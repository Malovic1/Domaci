//: Playground - noun: a place where people can play

import UIKit

typealias Player = [String : Any]
typealias TeamAndPlayers = [String : Any]

enum Team {
    case fkCZ
    case fkPartizan
}

enum PlayerPosition {
    case goalkeeper
    case defender
    case midfielder
    case attacker
}



func createPlayer(team: Team, position: PlayerPosition, playerNumber: Int, numberOfYellowCards: Int, redCardActive: Bool) -> Player {
    return ["team"                  : team,
            "position"              : position,
            "player number"         : playerNumber,
            "nuber of yellow cards" : numberOfYellowCards,
            "red card active"       : redCardActive]
}

func createTeam(team: Team) -> TeamAndPlayers {
    
    var playerList = [Player]()
    
    for i in 1...22 {
        
        var position: PlayerPosition
        
        switch i {
        case _ where i < 4:  position = .goalkeeper
        case _ where i < 9: position = .defender
        case _ where i < 14: position = .midfielder
        default: position = .attacker
        }
        
        
        var numberOfYellowCards = Int()
        var redCardActive = Bool()
        
        switch i {
        case 1, 5, 14:
            numberOfYellowCards = 3
            redCardActive = false
        case 9:
            numberOfYellowCards = 0
            redCardActive = true
        case 2, 10, 12:
            numberOfYellowCards = 4
        default:
            numberOfYellowCards = 0
            redCardActive = false
        }
       
        let player = createPlayer(team: team, position: position, playerNumber: i, numberOfYellowCards: numberOfYellowCards, redCardActive: redCardActive)
        playerList.append(player)
//        print("Team \(team) and player number \(i)")
    }
    
    return ["team name"   : team,
            "player list" : playerList]
}

func printPlayer(player: Player) {
    print(player)
}

func printPlayers(players: [Player]) {
    print(players)
}

func printPlayersWhoCanPlay(team: TeamAndPlayers) {
    let listOfPlayersFromTeam = team["player list"] as! [Player]
    var listOfPlayersWhoCanPlay = [Player]()
    
    for player in listOfPlayersFromTeam {
        if ((player["nuber of yellow cards"] as! Int) < 3) && ((player["red card active"] as! Bool) == false) {
            listOfPlayersWhoCanPlay.append(player)
        }
    }
    
    print(listOfPlayersWhoCanPlay)
}

let team1 = createTeam(team: .fkCZ)
let team2 = createTeam(team: .fkPartizan)

let team2Players = team2["player list"] as! [Player]
let team1Players = team1["player list"] as! [Player]
let randomPlayer = team1Players[7]
printPlayer(player: randomPlayer)
printPlayers(players: team1Players)
printPlayers(players: team2Players)
printPlayersWhoCanPlay(team: team1)




